# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://gasperbertoncelj@bitbucket.org/gasperbertoncelj/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/c91b18cf61d9ee0bee7c094e26b691fd582fe852

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/b3d78102e76cbe9eb1304da35af02a02025b1a87

Naloga 6.3.2:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/cc59db751dca837d8247d7bd1d8415192f56850d

Naloga 6.3.3:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/711aedfe792ed4d8fa3cd61cd0174c03f76755ea

Naloga 6.3.4:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/f99407b2a5cc581ef7c9349c5676b25a6517cef4

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/51c81fc2591a8d7da5faa84eb6eb384a145a76dc

Naloga 6.4.2:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/f5123053142e289da6636b271db9823920dd584f

Naloga 6.4.3:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/28bab97ba668499fbf8402a7c8510c917c6f5264

Naloga 6.4.4:
https://bitbucket.org/gasperbertoncelj/stroboskop/commits/7e97bbcaaf5b5ee16144c278000ca332efcbb5e3